# Hausbaumanager

Eine App zur Verwaltung, Organisation und Planung des Eigenheims

## Beschreibung

Es handelt sich hierbei um die API für die Landing Page der App.
Sie speichert die eingegebenen Email-Adressen in einer MongoDB-Datenbank.

### Stack

- [Node.js](https://nodejs.org/en/)
- [Express](https://expressjs.com/)
- [MongoDB](https://www.mongodb.com/)
- [TypeScript](https://www.typescriptlang.org/)

### Packages

- [Mongoose](https://mongoosejs.com/)
- [Cors](https://www.npmjs.com/package/cors)
- [dotenv](https://www.npmjs.com/package/dotenv)
- [jest](https://www.npmjs.com/package/jest)
- [ts-jest](https://github.com/kulshekhar/ts-jest)
- [supertest](https://www.npmjs.com/package/supertest)

### Installation

dev:

```bash
npm install
npm run dev
```

oder

```bash
yarn install
yarn dev
```

test:

```bash
yarn test
```

production:

```bash
npm run build
```

oder

```bash
yarn build
```
