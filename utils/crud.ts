import models from "../models";
import mongoose from "mongoose";

class Model {
  model: typeof mongoose.Model;
  constructor(model: typeof mongoose.Model) {
    this.model = model;
  }

  getById = async (id: string) => {
    return this.model.findById(id).exec();
  };

  getAll = async () => {
    return this.model.find().exec();
  };

  create = async (instance: any) => {
    instance.save((err: any) => {
      if (err) {
        console.log(err);
      }
    });
  };

  update = async (id: string, instance: any) => {
    this.model.findByIdAndUpdate(id, instance, (err: any) => {
      if (err) {
        console.log(err);
      }
    });
  };

  delete = async (id: string) => {
    return this.model.findByIdAndRemove(id).exec();
  };
}

const crud = {
  email: new Model(models.email),
};

export default crud;
