import mongoose from "mongoose";

export const connectDb = async (env: any) => {
  const CONNECTION_STRING: string = `mongodb://${env.MONGO_DB_HOST}:${env.MONGO_DB_PORT}/${env.MONGO_DB_NAME}`;
  return mongoose.connect(CONNECTION_STRING);
};

export const disconnectDb = async () => {
  return mongoose.disconnect();
};
