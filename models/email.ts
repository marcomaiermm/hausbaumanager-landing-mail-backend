import mongoose from "mongoose";
import { Email as EmailSchema } from "../schemas";

export const Email = mongoose.model("Email", EmailSchema);
