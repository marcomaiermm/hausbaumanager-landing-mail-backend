import { Email } from "./email";

const models = {
  email: Email,
};

export default models;
