import crud from "../utils/crud";
import models from "../models";
import express, { Request, Response } from "express";
import { validateEmail } from "../utils";

const create = async (req: Request, res: Response) => {
  const { email } = req.body;

  if (!validateEmail(email)) {
    res.status(400).json({
      message: "Invalid email",
    });
    return;
  }

  const ip = req.headers["x-forwarded-for"] || req.socket.remoteAddress;
  const payload = {
    email: email,
    createdAt: new Date(),
    ip: ip,
  };
  const instance = new models.email(payload);
  try {
    await crud.email.create(instance);
    res.status(200).json({
      message: "Email created successfully",
    });
  } catch {
    res.status(500).json({
      message: "Error creating email",
    });
  }
};

const getAll = async (req: Request, res: Response) => {
  try {
    //unauthorized to view emails
    res.status(401).json({ message: "Unauthorized" });


    // const emails = await crud.email.getAll();
    // res.status(200).json({
    //   message: "Emails retrieved successfully",
    //   emails: emails,
    // });
  } catch {
    res.status(500).json({
      message: "Error retrieving emails",
    });
  }
};

const router = express.Router();
router.get("/email", getAll);
router.post("/email", create);

export default router;
