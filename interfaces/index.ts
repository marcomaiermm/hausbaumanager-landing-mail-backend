export { Email as IEmail } from "./email";
export interface IObject {
  [key: string]: any;
}
