export interface Email {
  ip: string;
  email: string;
  createdAt: Date;
}
