import request from "supertest";
import Server from "../core";
import mongoose from "mongoose";

describe("GET /email", () => {
  let db: mongoose.Connection;
  const server = new Server();

  beforeAll(async () => {
    await mongoose.connect(
      `mongodb://${server.env.MONGO_DB_HOST}:${server.env.MONGO_DB_PORT}/test_hausbau_emails_landing`
    );
    db = mongoose.connection;
  });
  afterAll(async () => {
    await db.dropDatabase();
    await db.close();
  });
  it("should return status 200 and all mails in db", async () => {
    const response = await request(server.server).get("/email");
    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("emails");
    expect(response.body.emails).toBeInstanceOf(Array);
  });
  it("should return status 200 and create a new email entry in db", async () => {
    const response = await request(server.server).post("/email").send({
      email: "test@hausbaumanager.app",
    });
    expect(response.status).toBe(200);
  });
  it("should return status 400 and error message for invalid email", async () => {
    const response1 = await request(server.server).post("/email").send({
      email: "test",
    });
    const response2 = await request(server.server).post("/email").send({
      email: "",
    });
    const response3 = await request(server.server).post("/email").send({
      email: "test@@hausbaumanager.app",
    });

    expect(response1.status).toBe(400);
    expect(response2.status).toBe(400);
    expect(response3.status).toBe(400);
  });
});
