import { validateEmail } from "../utils/validators";

describe("Validate email", () => {
  it("should return true for valid email", () => {
    expect(validateEmail("")).toBe(false);
    expect(validateEmail("test@email.com")).toBe(true);
    expect(validateEmail("abc@@email.com")).toBe(false);
    expect(validateEmail("aa@e")).toBe(false);
  });
});
