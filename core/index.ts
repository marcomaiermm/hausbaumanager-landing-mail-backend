import dotenv from "dotenv";
import cors from "cors";
import express from "express";
import router from "../routes";
import { IObject } from "../interfaces";
import { connectDb } from "../services";

class Server {
  env: IObject;
  server: express.Application;
  constructor() {
    dotenv.config();
    this.env = process.env;
    this.server = express();
    this.server.use(cors());
    this.server.use(express.json());
    this.server.use(router);
  }
  run = async () => {
    const port = this.env.PORT || 3000;
    const app = this.server;
    connectDb(this.env).then(() => {
      app.listen(port, () => {
        console.log(`Server is running on port ${port} 🚀`);
      });
    });
  };
}

export default Server;
