import Server from "./core";
import { connectDb } from "./services/db";

const server = new Server();
server.run();
