import mongoose from "mongoose";

const Schema = mongoose.Schema;

const Email = new Schema({
  email: String,
  createdAt: Date,
  ip: String,
});

export { Email };
